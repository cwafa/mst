<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Mstpajak
 *
 * @author adi
 */
class Mstpajak extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('mstpajak/submit'),
            'add' => site_url('mstpajak/add'),
            'edit' => site_url('mstpajak/edit'),
            'delete' => site_url('mstpajak/delete'),
            'reload' => site_url('mstpajak'),
        );
        $this->load->model('mstpajak_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

  	public function json_dgview() {
  			echo $this->mstpajak_qry->json_dgview();
  	}

  	public function getKdAtribut() {
      echo $this->mstpajak_qry->getKdAtribut();
  	}

  	public function select_kddata() {
      echo $this->mstpajak_qry->select_kddata();
  	}

  	public function addData() {
      echo $this->mstpajak_qry->addData();
  	}

  	public function updateData() {
      echo $this->mstpajak_qry->updateData();
  	}

  	public function delete() {
      echo $this->mstpajak_qry->delete();
  	}

    private function _init_add(){
        $this->data['form'] = array(
           'tahun'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'tahun',
                    'name'        => 'tahun',
                    'value'       => date('Y'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
            ),
           'nama_kpp'=> array(
                    'placeholder' => 'Nama KPP',
                    'id'          => 'nama_kpp',
                    'name'        => 'nama_kpp',
                    'value'       => set_value('nama_kpp'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
            ),
           'lokasi_kpp'=> array(
                    'placeholder' => 'Lokasi KPP',
                    'id'          => 'lokasi_kpp',
                    'name'        => 'lokasi_kpp',
                    'value'       => set_value('lokasi_kpp'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
            ),
        );
    }
}
