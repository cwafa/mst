<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => 'blank'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes);
                ?>
                <div class="box-header">
                  <div class="col-md-3" style="padding: 0;">
                              <?php
                                  echo form_label('Periode');
                                  echo form_input($form['tahun']);
                                  echo form_error('tahun','<div class="note">','</div>');
                              ?>
                  </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
                    </div>
                    <div class="col-md-3" style="padding: 0;">
                        <button type="button" class="form-control btn btn-primary btn-tambah" data-toggle="modal" data-target="#modal_transaksi"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-striped dataTable">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Atribute</th>
                        <th>Target</th>
                        <th>Realisasi</th>
                        <th>Ket</th>
                        <th style="text-align: center;">Edit</th>
                        <th style="text-align: center;">Delete</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
<!--                    <button type="submit" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Cetak Bukti Potong PPh</button>-->
                <?php echo form_close(); ?>

    </div>
</div>


<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
          <button type="button" class="close" data-dismiss="modal" onclick="clear()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 style="display:block; text-align:center;">Tambah Data Perpajakan</h3>

          <form id="form-tambah-atribute">
            <div class="form-group">
              <input type="hidden" class="form-control" placeholder="Target" name="kddata" id="kddata" aria-describedby="sizing-addon2" readonly>
              <select class="form-control box" id="kdatribut">
                <option>Pilih Atribut</option>
              </select>
            </div>
            <div class="input-group form-group">
              <span class="input-group-addon" id="sizing-addon2">
                <i class="glyphicon glyphicon-record"></i>
              </span>
              <input type="text" class="form-control" placeholder="Target" name="tg_atribute" id="tg_atribute" onchange="ket()" aria-describedby="sizing-addon2">
            </div>
            <div class="input-group form-group">
              <span class="input-group-addon" id="sizing-addon2">
                <i class=" glyphicon glyphicon-save"></i>
              </span>
              <input type="text" class="form-control" placeholder="Realisasi" name="rl_atribute" id="rl_atribute" onchange="ket()" aria-describedby="sizing-addon2">
            </div>
            <div class="input-group form-group">
              <span class="input-group-addon" id="sizing-addon2">
                <i class=" glyphicon glyphicon-comment"></i>
              </span>
              <input type="text" class="form-control" placeholder="Keterangan" name="ket" id="ket" onchange="ket()" aria-describedby="sizing-addon2" readonly>
              <input type="hidden" class="form-control" placeholder="Keterangan" name="periode" id="periode" readonly>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                  <button type="button" class="form-control btn btn-primary btn-add"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
                  <button type="button" class="form-control btn btn-primary btn-update"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
              </div>
            </div>
          </form>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $(".btn-add").show();
        $(".btn-update").hide();
        $("#tg_atribute").autoNumeric();
        $("#rl_atribute").autoNumeric();
        getKdAtribut();
        clear();

        $('#tahun').datetimepicker({
          format : 'YYYY'
         });

        var column = [];

        column.push({
            "aTargets": [  ],
            "mRender": function (data, type, full) {
              return moment(data).isValid() ? type === 'export' ? data : moment(data).format('L') : data;
//                return type === 'export' ? data : moment(data).format('L');
          },
          "sClass": "center"
          });

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
                // return formmatedvalue;
              },
              "sClass": "right"
            });
        column.push({
                "aTargets": [ 1,5,6 ],
          			"searchable": false,
                "orderable": false,
                "sClass": "center",
            });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "targets": 0 , "data": null,"sortable": false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "nmatribut" },
                { "data": "tg_atribut" },
                { "data": "rl_atribut" },
                { "data": "ket" },
                { "data": "edit" },
                { "data": "delete" }
            ],
            //"lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                                { "name": "tahun", "value": $("#tahun").val() }
                            );
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('mstpajak/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            //dom: '<"html5buttons"B>lTfgitp',

            buttons: [
                {extend: 'copy',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'csv',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'excel',
                    exportOptions: {orthogonal: 'export'}},
                {extend: 'pdf',
                    orientation: 'landscape',
                    pageSize: 'A0'
                },
                {extend: 'print',
                    customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');
                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                   }
                }
            ],
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });

       $('.btn-tampil').click( function(){
           table.ajax.reload();
       });

       $(".btn-tambah").click(function(){
         $('.btn-add').show();
         $('.btn-update').hide();
         select_kddata();
         getKdAtribut();
         $('#kdatribut').select2({
             placeholder: '-- Pilih Kode Akun --',
             dropdownAutoWidth : true,
             width: '100%',
             escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
           //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
           //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
         });
         clear();
       });

       $(".btn-add").click(function(){
         //alert($('#kddata').val());
         addData();
       });

       $(".btn-update").click(function(){
            updateData();
       });

       $("#tg_atribute").keyup(function(){
          ket();
       });

       $("#rl_atribute").keyup(function(){
          ket();
       });
    });

    function getKdAtribut(){
        $.ajax({
            type: "POST",
            url: "<?=site_url("mstpajak/getKdAtribut");?>",
            data: {},
            beforeSend: function() {
                $('#kdatribut').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Atribut --'));
                $("#kdatribut").trigger("change.chosen");
                if ($('#kdatribut').hasClass("chosen-hidden-accessible")) {
                    $('#kdatribut').select2('destroy');
                    $("#kdatribut").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kdatribut')
                        .append($('<option>', { value : value.kdatribut })
                        .html("<b style='font-size: 14px;'>" + value.kdatribut + " - " + value.nmatribut + " </b>"));
                });
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }

    function clear(){
        $("#kddata").val('');
        $("#kdatribut").val('');
        $("#tg_atribute").autoNumeric('set', '');
        $("#rl_atribute").autoNumeric('set', '');
        $("#ket").val('');
        $("#periode").val('');
    }

    function ket(){
        var target = $("#tg_atribute").autoNumeric('get');
        var real = $("#rl_atribute").autoNumeric('get');
        var total = parseInt(target-real);
  //      alert(total);
        if (total<0){
              $("#ket").val("Tercapai");
        } else {
            $("#ket").val("Tidak Tercapai");
        }
    }

    function addData(){
          var kode = $("#kddata").val();
          var kddata = parseInt(kode)+1;
          var kdatribut = $("#kdatribut").val();
          var tg_atribute = $("#tg_atribute").autoNumeric('get');
          var rl_atribute = $("#rl_atribute").autoNumeric('get');
          var ket = $("#ket").val();
          $.ajax({
              type: "POST",
              url: "<?=site_url("mstpajak/addData");?>",
              data: {"kddata":kddata
                      ,"kdatribut":kdatribut
                      ,"tg_atribute":tg_atribute
                      ,"rl_atribute":rl_atribute
                      ,"ket":ket },
              success: function(){
                  $("#modal_transaksi").modal('hide');
                          swal({
                              title: "Proses Tambah Berhasil",
                              text: "Data"+ket+"",
                              type: "success"
                          });
                          table.ajax.reload();
              },
              error:function(event, textStatus, errorThrown) {
                  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  table.ajax.reload();
              }
          });
    }

    function updateData(){
          var kddata = $("#kddata").val();
          var kdatribut = $("#kdatribut").val();
          var tg_atribute = $("#tg_atribute").autoNumeric('get');
          var rl_atribute = $("#rl_atribute").autoNumeric('get');
          var ket = $("#ket").val();
          var periode = $("#periode").val();
          Tahun = new Date().getFullYear();
          if (periode==Tahun){
            $.ajax({
                type: "POST",
                url: "<?=site_url("mstpajak/updateData");?>",
                data: {"kddata":kddata
                        ,"kdatribut":kdatribut
                        ,"tg_atribute":tg_atribute
                        ,"rl_atribute":rl_atribute
                        ,"ket":ket },
                success: function(){
                    $("#modal_transaksi").modal('hide');
                            swal({
                                title: "Proses Update Berhasil",
                                text: "Data"+ket+"",
                                type: "success"
                            });
                            table.ajax.reload();
                            clear();
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    table.ajax.reload();
                    clear();
                }
            });
          } else {
            swal({
                title: "Proses Gagal",
                text: "Data Telah di Closing",
                type: "warning"
            });
            clear();
          }
    }

    function edit(kddata,kdatribut,tg_atribut,rl_atribut,ket,periode){
        $("#kddata").val(kddata);
        $("#kdatribut").val(kdatribut);
        $("#kdatribut").trigger("chosen:updated");
        $('#kdatribut').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });
        $("#tg_atribute").autoNumeric('set',tg_atribut);
        $("#rl_atribute").autoNumeric('set',rl_atribut);
        $("#ket").val(ket);
        $("#periode").val(periode);
        $('#modal_transaksi').modal('toggle');
            $(".btn-add").hide();
            $(".btn-update").show();

        }

        function select_kddata(){
            $.ajax({
                type: "GET",
                url: "<?=site_url("mstpajak/select_kddata");?>",
                data: {},
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                            $("#kddata").val(data.jml);
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
    }

    function deleted(kddata,periode){
    Tahun = new Date().getFullYear();
    if (periode==Tahun){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("mstpajak/delete");?>",
                    data: {"kddata":kddata },
                    success: function(resp){
                                swal({
                                    title: "Proses Hapus Berhasil",
                                    text: "Data Berhasil Dihapus",
                                    type: "success"
                                });
                        table.ajax.reload();
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
      } else {
        swal({
            title: "Proses Gagal",
            text: "Data Telah di Closing",
            type: "warning"
        });
        clear();
      }
    }
</script>
