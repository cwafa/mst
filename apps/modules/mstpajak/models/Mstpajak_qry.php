<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of mstpajak_qry
 *
 * @author adi
 */
class mstpajak_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    public function __construct() {
        parent::__construct();
    }

  	public function getKdAtribut() {
  			$this->db->select("kdatribut,nmatribut");
  			$this->db->where("faktif",'true');
  			$query = $this->db->get('atribut');
  			if($query->num_rows()>0){
  					$res = $query->result_array();
  			}else{
  					$res = false;
  			}
  			return json_encode($res);
  	}


  		public function addData(){
  			$kddata = $this->input->post('kddata');
  			$kdatribut = $this->input->post('kdatribut');
  			$ket = $this->input->post('ket');
  			$tg_atribut = $this->input->post('tg_atribute');
  			$rl_atribut = $this->input->post('rl_atribute');
  			$tgl=date('Y-m-d');

  		 	$data = array(
  			 		'kddata' => $kddata,
  			 		'kdatribut' => $kdatribut,
  			 		'tg_atribut' => $tg_atribut,
  			 		'rl_atribut' => $rl_atribut,
  			 		'ket' => $ket,
  			 		'periode' => $tgl
  			 );

  			 	$q = $this->db->insert("d_attr", $data);
  			 	$q2 = $this->db->insert("d2_attr", $data);
  				$q3 = $this->db->insert("d3_attr", $data);

  		}


  	public function json_dgview() {
  			error_reporting(-1);
  			if( isset($_GET['tahun']) ){
  				if($_GET['tahun']){
  					//$tgl1 = explode('-', $_GET['periode_awal']);
  					$tahun = $_GET['tahun'];//$tgl1[1].$tgl1[0];
  				}else{
  					$tahun = '';
  				}
  			}else{
  				$tahun = '';
  			}
  			$aColumns = array('no', 'nmatribut', 'tg_atribut', 'rl_atribut', 'ket', 'kdatribut', 'kddata', 'periode' );
  		$sIndexColumn = "kdatribut";
  			$sLimit = "";
  			if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
  					$sLimit = " LIMIT " . $_GET['iDisplayLength'];
  			}
  			$sTable = " ( SELECT '' as no, a.kddata, a.kdatribut, b.nmatribut, a.tg_atribut, a.rl_atribut, a.ket, year(a.periode) as periode FROM d_attr a join atribut b on a.kdatribut=b.kdatribut where year(a.periode)='".$tahun."') AS a";
  			if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
  			{
  					if($_GET['iDisplayStart']>0){
  							$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
  											intval( $_GET['iDisplayStart'] );
  					}
  			}

  			$sOrder = "";
  			if ( isset( $_GET['iSortCol_0'] ) )
  			{
  							$sOrder = " ORDER BY  ";
  							for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
  							{
  											if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
  											{
  															$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
  																			($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
  											}
  							}

  							$sOrder = substr_replace( $sOrder, "", -2 );
  							if ( $sOrder == " ORDER BY" )
  							{
  											$sOrder = "";
  							}
  			}
  			$sWhere = "";

  			if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
  			{
  	$sWhere = " WHERE (";
  	for ( $i=0 ; $i<count($aColumns) ; $i++ )
  	{
  		$sWhere .= "LOWER(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
  	}
  	$sWhere = substr_replace( $sWhere, "", -3 );
  	$sWhere .= ')';
  			}


  			/*
  			 * SQL queries
  			 */

  			if(empty($sOrder)){
  					$sOrder = " order by kdatribut ";
  			}


  			$sQuery = "
  							SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
  							FROM   $sTable
  							$sWhere
  							$sOrder
  							$sLimit
  							";

  			//echo $sQuery;

  			$rResult = $this->db->query( $sQuery);

  			$sQuery = "
  							SELECT COUNT(".$sIndexColumn.") AS jml
  							FROM $sTable
  							$sWhere";    //SELECT FOUND_ROWS()

  			$rResultFilterTotal = $this->db->query( $sQuery);
  			$aResultFilterTotal = $rResultFilterTotal->result_array();
  			$iFilteredTotal = $aResultFilterTotal[0]['jml'];

  			$sQuery = "
  							SELECT COUNT(".$sIndexColumn.") AS jml
  							FROM $sTable
  							$sWhere";
  			$rResultTotal = $this->db->query( $sQuery);
  			$aResultTotal = $rResultTotal->result_array();
  			$iTotal = $aResultTotal[0]['jml'];

  			$output = array(
  							"sEcho" => intval($_GET['sEcho']),
  							"iTotalRecords" => $iTotal,
  							"iTotalDisplayRecords" => $iFilteredTotal,
  							"data" => array()
  			);

  			foreach ( $rResult->result_array() as $aRow )
  			{
  					foreach ($aRow as $key => $value) {
  							if(is_numeric($value)){
  									$aRow[$key] = (float) $value;
  							}else{
  									$aRow[$key] = $value;
  							}
  					}

  					$aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kddata']."','".$aRow['kdatribut']."'
  					,'".$aRow['tg_atribut']."','".$aRow['rl_atribut']."','".$aRow['ket']."','".$aRow['periode']."');\">Edit</button>";
  					$aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kddata']."','".$aRow['periode']."');\">Hapus</button>";

  					$output['data'][] = $aRow;
  			}
  			echo  json_encode( $output );

  	}

  	public function updateData() {
  		$kdatribut = $this->input->post('kdatribut');
  		$ket = $this->input->post('ket');
  		$kddata = $this->input->post('kddata');
  		$tg_atribute = $this->input->post('tg_atribute');
  		$rl_atribute = $this->input->post('rl_atribute');
  		$tgl=date('Y-m-d');

  				$q = $this->db->query("UPDATE d_attr set kdatribut='".$kdatribut."', tg_atribut=".$tg_atribute.", rl_atribut=".$rl_atribute.",
  						ket='".$ket."', periode='".$tgl."' WHERE kddata='".$kddata."'");

  				$q2 = $this->db->query("UPDATE d2_attr set kdatribut='".$kdatribut."', tg_atribut=".$tg_atribute.", rl_atribut=".$rl_atribute.",
  						ket='".$ket."', periode='".$tgl."' WHERE kddata='".$kddata."'");

  				$q3 = $this->db->query("UPDATE d3_attr set kdatribut='".$kdatribut."', tg_atribut=".$tg_atribute.", rl_atribut=".$rl_atribute.",
  						ket='".$ket."', periode='".$tgl."' WHERE kddata='".$kddata."'");
  			//echo $this->db->last_query();

  	}

  	public function delete() {
  			$kddata = $this->input->post('kddata');

  			$q = $this->db->query("DELETE FROM d_attr WHERE kddata='".$kddata."'");
  			$q2 = $this->db->query("DELETE FROM d2_attr WHERE kddata='".$kddata."'");
  			$q3 = $this->db->query("DELETE FROM d3_attr WHERE kddata='".$kddata."'");
  			//echo $this->db->last_query();

  	}

  	public function select_kddata() {
  					//$query = $this->db->query('select count(kddata) from d_attr');
  	        $query = $this->db->query('select count(kddata) as jml from d_attr');
  	        if($query->num_rows()>0){
  	            $res = $query->result_array();
  	        }else{
  	            $res = false;
  	        }
  	    return json_encode($res);
  		}
}
