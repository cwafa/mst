<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashboard
 *
 * @author adi
 */
class Dashboard extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
        );
        $this->load->model('dashboard_qry');
    }

    //redirect if needed, otherwise display the user list

    public function index()
    {
        $this->data['jml_pajak'] 	= $this->dashboard_qry->total_rows_pajak();
        $this->_init_add();
        $this->template
            ->title('Dashboard',$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
        $this->data['jml_attr'] 	= $this->dashboard_qry->total_rows_attr();
        $this->template
            ->title('Dashboard',$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);


          		//$data['jml_posisi'] 	= $this->dashboard_qry->total_rows();
          		//$data['jml_kota'] 		= $this->dashboard_qry->total_rows();
          		//$data['userdata'] 		= $this->userdata;

          		//$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

          		/*$posisi 				= $this->dashboard_qry->select_all();
          		$index = 0;
          		foreach ($posisi as $value) {
          		    $color = '#' .$rand[rand(0,15)] .$rand[rand(0,15)] .$rand[rand(0,15)] .$rand[rand(0,15)] .$rand[rand(0,15)] .$rand[rand(0,15)];

          			$pegawai_by_posisi = $this->dashboard_qry->select_by_id($value->kdatribut);

          			$data_posisi[$index]['value'] = $pegawai_by_posisi->jml;
          			$data_posisi[$index]['color'] = $color;
          			$data_posisi[$index]['highlight'] = $color;
          			$data_posisi[$index]['label'] = $value->nmatribut;

          			$index*/
          		//}

          		//$data['data_posisi'] = json_encode($data_posisi);

          		//$data['page'] 			= "home";
          		//$data['judul'] 			= "Beranda";
          		//$data['deskripsi'] 		= "Manage Data CRUD";
          		//$this->template->views('home', $data);
    }

    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('Y-01'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('Y-m'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
        );
    }
}
