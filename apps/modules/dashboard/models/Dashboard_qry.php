<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Dashboard_qry
 *
 * @author adi
 */
class Dashboard_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

  	public function total_rows_pajak() {
  		$data = $this->db->query('select * from d_attr where year(periode)=year(now())');
  		return $data->num_rows();
  	}

  	public function total_rows_attr() {
  		$data = $this->db->query('select * from atribut where faktif="true"');
  		return $data->num_rows();
  	}
}
