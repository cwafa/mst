<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('access_qry');
        
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");        
    }

    public function index(){
        $logged_in = $this->session->userdata('logged_in');
        if($logged_in){
            redirect("dashboard");
        }else{            
            $this->template
                ->title('Login User',$this->apps->name)
                ->set_layout('access')
                ->build('index');
        }              
    }
    
    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        if($this->_validate($username,$password) == TRUE){
            $res = $this->access_qry->submit();
            if($res){
                redirect("dashboard");
            }else{            
                $this->template
                    ->title('Login User',$this->apps->name)
                    ->set_layout('access')
                    ->build('index');
            }   
        }else{
            $this->template
                ->title('Login User',$this->apps->name)
                ->set_layout('access')
                ->build('index');
        }
    }
    
    public function logout() {
        $this->session->sess_destroy();
        redirect('access','refresh');
    }    
    
    private function _validate($username,$password) {
        if(!empty($username) && !empty($password)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'username',
                    'label' => 'User Name',
                    'rules' => 'required|alpha_numeric_spaces|max_length[25]',
                ),
            array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required|alpha_numeric_spaces|max_length[25]',
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

}
