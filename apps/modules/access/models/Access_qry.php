<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Access_qry
 *
 * @author adi
 */
class Access_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function submit() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

/*         $username = "ADMINISTRATOR";
        $password = "1234";	 */
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        if(strtoupper($username)=="ADMIN" && $password=="1234"){
            $newdata = array(
                'username'  => strtoupper($username),
                'nmuser'  => ucwords(strtolower($username)),
                'kddiv'  => '-',
                'nmlokasi'  => '-',
                'jenis'  => '-',
                'logged_in' => TRUE,
                'platform' => $this->agent->platform(),
                'browser' => $agent,
            );
            $this->session->set_userdata($newdata);
            return true;
        }else{
            return false;
        }

    }
}
