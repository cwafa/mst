<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptpajak_qry
 *
 * @author adi
 */
class Rptpajak_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }
/*
    public function submit() {
        $array = $this->input->post();
        $array['periode_awal'] = $this->apps->dateConvert($array['periode_awal']);
        $array['periode_akhir'] = $this->apps->dateConvert($array['periode_akhir']);

        $th_awal = $array['periode_awal'];
        $th_akhir = $array['periode_akhir'];
        $ket = $th_akhir - $th_awal;
      	 if($ket=='2'){
              $thmid = $th_akhir-1;
      			  $query = "SELECT kdatribut, nmatribut,
                          (select tg_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as target_".$th_awal.",
                          (select rl_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as realisasi_".$th_awal.",
                          (select ket from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as ket_".$th_awal." ,
                          (select tg_atribut from d2_attr where d2_attr.kdatribut=atribut.kdatribut AND year(periode)='".$thmid."')as target_".$thmid." ,
                          (select rl_atribut from d2_attr where d2_attr.kdatribut=atribut.kdatribut AND year(periode)='".$thmid."')as realisasi_".$thmid." ,
                          (select ket from d2_attr where d2_attr.kdatribut=atribut.kdatribut AND year(periode)='".$thmid."')as ket_".$thmid.",
                          (select tg_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as target_".$th_akhir." ,
                          (select rl_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as realisasi_".$th_akhir." ,
                          (select ket from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as ket_".$th_akhir." from atribut ";
      			}else{
              $query = "SELECT kdatribut, nmatribut,
                          (select tg_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as target_".$th_awal.",
                          (select rl_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as realisasi_".$th_awal.",
                          (select ket from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as ket_".$th_awal." ,
                          (select tg_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as target_".$th_akhir." ,
                          (select rl_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as realisasi_".$th_akhir." ,
                          (select ket from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as ket_".$th_akhir." from atribut ";
      			}

      			$q = $this->db->query($query);

      			if($q->num_rows()>0){
      					$res = $q->result_array();
      			}else{
      					$res = null;
      			}
      			return json_encode($res);

    }*/



    public function submit() {
        $array = $this->input->post();

        $th_awal = $array['periode_awal'];
        $th_akhir = $th_awal-1;

  			$del = $this->db->query("DELETE FROM total");

        $query = "SELECT kdatribut, nmatribut,
                        (select tg_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as t1,
                        (select rl_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as r1,
                        (select ket from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as k1 ,
                        (select tg_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as t2 ,
                        (select rl_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as r2 ,
                        (select ket from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as k2 ,
                        (select case when sum((t1+t2)-(r1+r2))<=0 then 'tc' else 'tt' end) as ket,
                        (select count(ket) where ket='tc')as tot from atribut where faktif='true'";

        $ins = $this->db->query("INSERT INTO total (kd,nm,t1,r1,k1,t2,r2,k2,ket) SELECT kdatribut, nmatribut,
                        (select tg_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as t1,
                        (select rl_atribut from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as r1,
                        (select ket from d_attr where d_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_awal."')as k1 ,
                        (select tg_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as t2 ,
                        (select rl_atribut from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as r2 ,
                        (select ket from d3_attr where d3_attr.kdatribut=atribut.kdatribut AND year(periode)='".$th_akhir."')as k2 ,
                        (select case when sum((t1+t2)-(r1+r2))<=0 then 'tc' else 'tt' end) as ket from atribut where faktif='true'");

        $q = $this->db->query($query);
        $res = $q->result_array();
        $data = array();

          foreach ( $res as $aRow )
          {
              foreach ($aRow as $key => $value) {
                  if(is_numeric($value)){
                      $aRow[$key] = (float) $value;
                  }else{
                      $aRow[$key] = $value;
                  }
              }
              $data[] = $aRow;
          }

          return $data;
    }

    public function total() {
        $array = $this->input->post();

        $th_awal = $array['periode_awal'];

        $count = "SELECT count(kd) as jumlah, (select count(ket) from total where ket='tc') as jml_tc, (select count(k1) from total where ket='tc') as tot_k1,
                   (select count(k2) from total where ket='tc') as tot_k2,
                   (select count(k1) from total where ket='tt') as tot_g1,
                   (select count(k2) from total where ket='tt') as tot_g2 from total";

      	$c = $this->db->query($count);
        $res = $c->result_array();
        $data2 = array();
          foreach ( $res as $aRow )
          {
              foreach ($aRow as $key => $value) {
                  if(is_numeric($value)){
                      $aRow[$key] = (float) $value;
                  }else{
                      $aRow[$key] = $value;
                  }
              }
              $data2[] = $aRow;
          }
          return $data2;
    }

}
