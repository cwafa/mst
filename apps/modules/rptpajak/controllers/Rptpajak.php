<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Rptpajak
 *
 * @author adi
 */
class Rptpajak extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('rptpajak/submit'),
            'add' => site_url('rptpajak/add'),
            'edit' => site_url('rptpajak/edit'),
            'reload' => site_url('rptpajak'),
        );
        $this->load->model('rptpajak_qry');
        /*$akun = $this->rptpajak_qry->getAkun();
        foreach ($akun as $value) {
            $this->data['akun'][$value['kdakun']] = $value['kdakun']." - ".$value['nmakun'];
        }*/
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function submit() {
        $this->data['data'] = $this->rptpajak_qry->submit();
        $this->data2['data2'] = $this->rptpajak_qry->total();
        $array = $this->input->post();
        if($array['submit']==="html" || $array['submit']==="excel"){
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('html',$this->data);
            $this->template
                ->title($this->data['msg_main'],$this->apps->name)
                ->set_layout('print-layout')
                ->build('html',$this->data2);
        }else{
              redirect("rptpajak");
        }
    }

    public function printAll() {
        $this->data['data'] = $this->mstrefposkb_qry->printAll();
        $this->data2['data2'] = $this->mstrefposkb_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
        $this->template
            ->title($this->data2['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data2);
    }

    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('Y'),
                    'class'       => 'form-control',
            ),
        );
    }
}
