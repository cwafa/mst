<?php

/*
 * ***************************************************************
 * Script : pdf
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #2c2c2c;
        text-align: center;
    }
    body{
        overflow-x: auto;
    }
</style>
<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3800);

$array = $this->input->post();
$kettahun=$array['periode_awal'] - 1;
if($array['submit']==="excel"){
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAP_KATEGORI_".date('Ymd_His').".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}
$this->load->library('table');

$template = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);

$caption = "<b>LAPORAN TAHUNAN</b>";
$caption .= "<br>"
            . "<b>". strtoupper($this->apps->title)."</b>"
            . "<br>"
            . "<b> PERIODE " .$kettahun .' S/D '.$array['periode_awal'] ."</b>"
            . "<br><br>";

$namaheader = array(
    array('data' => 'No.'
                , 'style' => 'text-align: center; width: 5%; font-size: 12px;'),
    array('data' => 'Nama Atribut'
                , 'colspan' => 2
                , 'style' => 'text-align: center; width: 15%; font-size: 12px;'),
    array('data' => 'Target '.$kettahun.''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Realisasi '.$kettahun.''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Keterangan '.$kettahun.''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Target '.$array['periode_awal'].''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Realisasi '.$array['periode_awal'].''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Keterangan '.$array['periode_awal'].''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'Klasifikasi'
                , 'style' => 'text-align: center; width: 10%; font-size: 12px;'),
      );

// Caption text
$this->table->set_caption($caption);
$this->table->add_row($namaheader);
$no = 1;
foreach ($data as $value) {
    //var_dump($value['detail']);
    if ($value['ket'] == 'tc'){
      $ket= "Tercapai";
    } else {
      $ket= "Belum Tercapai";
    }
    $header_data = array(
        array('data' => $no
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['nmatribut']
                            , 'colspan' => 2
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $value['t1']
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => $value['r1']
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => $value['k1']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['t2']
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => $value['r2']
                            , 'style' => 'text-align: right; font-size: 12px;'),
        array('data' => $value['k2']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $ket
                            , 'style' => 'text-align: center; font-size: 12px;'),
    );
    $this->table->add_row($header_data);
    $no++;
}

$this->table->set_template($template);
echo $this->table->generate();

$template2 = array(
        'table_open'            => '<table style="border-collapse: collapse;" width="100%" border="1" cellspacing="1">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
);

$namaheader2 = array(
    array('data' => 'No.'
                , 'style' => 'text-align: center; width: 5%; font-size: 12px;'),
    array('data' => 'Peluang'
                , 'colspan' => 2
                , 'style' => 'text-align: center; width: 15%; font-size: 12px;'),
    array('data' => ''.$kettahun.''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => ''.$array['periode_awal'].''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'P'.$kettahun.''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
    array('data' => 'P'.$array['periode_awal'].''
                , 'style' => 'text-align: center; width: 9%; font-size: 12px;'),
      );

      $this->table->add_row($namaheader2);
$no = 1;
foreach ($data2 as $value) {
  $sel = $value['jumlah'] - $value['jml_tc'];
  $sel16 = $value['jumlah'] - $value['tot_k1'];
  $sel17 = $value['jumlah'] - $value['tot_k2'];
//  $pnh = ($value['jml_tc'] / $value['jumlah']) * 100;
//  $tpnh = ($sel / $value['jumlah']) * 100;
//  $tot16 = ($value['tot_k1'] / $value['jumlah']) * 100;
//  $tot17 = ($value['tot_k2'] / $value['jumlah']) * 100;
//  $xsel16 = ($sel16 / $value['jumlah']) * 100;
//  $xsel17 = ($sel17 / $value['jumlah']) * 100;
//  $tot16 = ($value['tot_k1'] / $value['jumlah']) * 100;/
//  $tot17 = ($value['tot_k2'] / $value['jumlah']) * 100;
//  $xsel16 = ($sel16 / $value['jumlah']) * 100;
//  $xsel17 = ($sel17 / $value['jumlah']) * 100;
  $xpnh = ($value['jml_tc'] / $value['jumlah']) * 100;
  $pnh = ($value['jml_tc'] / $value['jumlah']);
  $tpnh = ($sel / $value['jumlah']);
  $tot16 = ($value['tot_k1'] / $value['jumlah']) ;
  $tot17 = ($value['tot_k2'] / $value['jumlah']) ;
  $xsel16 = ($sel16 / $value['jumlah']) ;
  $xsel17 = ($sel17 / $value['jumlah']) ;
  $tot16 = ($value['tot_k1'] / $value['jumlah']) ;
  $tot17 = ($value['tot_k2'] / $value['jumlah']) ;
  $xsel16 = ($sel16 / $value['jumlah']);
  $xsel17 = ($sel17 / $value['jumlah']);
  if ($xpnh>85){
    $klasifikasi = 'Sangat Memuaskan';
  } else if($xpnh>70){
    $klasifikasi = 'Memuaskan';
  } else {
    $klasifikasi = 'Kurang Memuaskan';
  }
  $caption2 = "<b>Hasil Klasifikasi = ".$klasifikasi."</b><br>";
  $caption2 .= "<b>Peluang Yang Terpenuhi = ".$pnh."</b><br>"
              . "<b>Peluang Yang Tidak Terpenuhi = ".$tpnh."</b><br>"
              . "<br><br>";
    //var_dump($value['detail']);

    $header_data = array(
        array('data' => '1'
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => 'Tercapai'
                            , 'colspan' => 2
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $value['tot_k1']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['tot_k2']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $tot16
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $tot17
                            , 'style' => 'text-align: center; font-size: 12px;'),
    );

    $header_data2 = array(
        array('data' => '2'
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => 'Tidak Tercapai'
                            , 'colspan' => 2
                            , 'style' => 'text-align: left; font-size: 12px;'),
        array('data' => $value['tot_g1']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $value['tot_g2']
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $xsel16
                            , 'style' => 'text-align: center; font-size: 12px;'),
        array('data' => $xsel17
                            , 'style' => 'text-align: center; font-size: 12px;'),
    );
    $this->table->add_row($header_data);
    $this->table->add_row($header_data2);
    $no++;
}

$this->table->set_caption($caption2);
$this->table->set_template($template2);
echo $this->table->generate();

$total_penuh=$pnh*$tot16*$tot17;
$total_notpenuh=$tpnh*$xsel16*$xsel17;
$namaheader2 = array();
$this->table->add_row($namaheader2);
$caption3 = "<b>Probabilitas Terpenuhi = ".$pnh." * ".$tot16." * ".$tot17." = ".$total_penuh."</b><br>";
$caption3 .= "<b>Probabilitas Tidak Terpenuhi = ".$tpnh." * ".$xsel16." * ".$xsel17." = ".$total_notpenuh."</b><br>"
            . "<br><br>";
$this->table->set_caption($caption3);
echo $this->table->generate();
