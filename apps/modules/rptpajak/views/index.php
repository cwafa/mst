<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?>
<style>
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: none;
    }
    .radio {
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 5px;
        font-weight: bold;
        cursor: pointer;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data'
                , 'target' => '_blank'
                , 'data-validate' => 'parsley');
            echo form_open($submit,$attributes);
        ?>
          <div class="box-body">
              <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                        <?php
                            echo form_label('Periode Awal');
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>');
                        ?>
                    </div>
                  </div>
              </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" name="submit" value="html">
                <i class="fa fa-print"></i> Cetak
            </button>
            <button type="submit" class="btn btn-success" name="submit" value="excel">
                <i class="fa fa-file-excel-o"></i> Export Ke Excel
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#periode_awal').datetimepicker({
          format : 'YYYY'
        });

        $('#periode_akhir').datetimepicker({
            format : 'YYYY'
        });
        $('.btn-cetak').click(function(){
          //  printAll();
        });
    });

    function printAll(){
        var thawal = $("#tahun1").val();
        var thakhir = $("#tahun2").val();
        var excel = $("#excel").val();
        var html = $("#html").val();
        var ket = parseInt(thakhir)-parseInt(thawal);
        if(ket='2'){
          var thmid = parseInt(thakhir-1);
        } else {
            var thmid = parseInt(thakhir-0);
        }
            $.ajax({
                  type: "GET",
                  url: "<?=site_url("Lap_PDF/printAll");?>",
                  data: {"thawal":thawal
                          ,"thakhir":thakhir
                          ,"thmid":thmid
                          ,"ket":ket
                          ,"excel":excel
                          ,"html":html  },
                  success: function(){
                  },
                  error:function(event, textStatus, errorThrown) {
                      swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  }
              });
        }
</script>
