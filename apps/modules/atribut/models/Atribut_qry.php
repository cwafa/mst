<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Atribut_qry
 *
 * @author adi
 */
class Atribut_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->apps->kd_cabang;
    }


    	public function addData(){
    		$kdatribut = $this->input->post('kdatribut');
    		$nmatribut = $this->input->post('nmatribut');
    		$faktif		= "true";

    	 $data = array(
    		 'kdatribut' => $kdatribut,
    		 'nmatribut' => $nmatribut,
    		 'faktif' => $faktif
    		 );

    			$q = $this->db->insert("atribut", $data);

    	}

    public function json_dgview() {
    		error_reporting(-1);

    		$aColumns = array('no', 'nmatribut', 'faktifx', 'kdatribut');
    	$sIndexColumn = "kdatribut";
    		$sLimit = "";
    		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
    				$sLimit = " LIMIT " . $_GET['iDisplayLength'];
    		}
    		$sTable = " ( SELECT '' as no, kdatribut, nmatribut,
            CASE
                WHEN faktif=true THEN 'AKTIF'
                ELSE 'TIDAK'
            END AS faktifx FROM atribut) AS a";
    		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    		{
    				if($_GET['iDisplayStart']>0){
    						$sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
    										intval( $_GET['iDisplayStart'] );
    				}
    		}

    		$sOrder = "";
    		if ( isset( $_GET['iSortCol_0'] ) )
    		{
    						$sOrder = " ORDER BY  ";
    						for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
    						{
    										if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
    										{
    														$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
    																		($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
    										}
    						}

    						$sOrder = substr_replace( $sOrder, "", -2 );
    						if ( $sOrder == " ORDER BY" )
    						{
    										$sOrder = "";
    						}
    		}
    		$sWhere = "";

    		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    		{
    $sWhere = " WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
    	$sWhere .= "LOWER(".$aColumns[$i].") LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
    		}


    		/*
    		 * SQL queries
    		 */

    		if(empty($sOrder)){
    				$sOrder = " order by kdatribut ";
    		}


    		$sQuery = "
    						SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
    						FROM   $sTable
    						$sWhere
    						$sOrder
    						$sLimit
    						";

    		//echo $sQuery;

    		$rResult = $this->db->query( $sQuery);

    		$sQuery = "
    						SELECT COUNT(".$sIndexColumn.") AS jml
    						FROM $sTable
    						$sWhere";    //SELECT FOUND_ROWS()

    		$rResultFilterTotal = $this->db->query( $sQuery);
    		$aResultFilterTotal = $rResultFilterTotal->result_array();
    		$iFilteredTotal = $aResultFilterTotal[0]['jml'];

    		$sQuery = "
    						SELECT COUNT(".$sIndexColumn.") AS jml
    						FROM $sTable
    						$sWhere";
    		$rResultTotal = $this->db->query( $sQuery);
    		$aResultTotal = $rResultTotal->result_array();
    		$iTotal = $aResultTotal[0]['jml'];

    		$output = array(
    						"sEcho" => intval($_GET['sEcho']),
    						"iTotalRecords" => $iTotal,
    						"iTotalDisplayRecords" => $iFilteredTotal,
    						"data" => array()
    		);

    		foreach ( $rResult->result_array() as $aRow )
    		{
    				foreach ($aRow as $key => $value) {
    						if(is_numeric($value)){
    								$aRow[$key] = (float) $value;
    						}else{
    								$aRow[$key] = $value;
    						}
    				}

    				$aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kdatribut']."','".$aRow['nmatribut']."'
    				,'".$aRow['faktifx']."');\">Edit</button>";
    				$aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['kdatribut']."');\">Hapus</button>";

    				$output['data'][] = $aRow;
    		}
    		echo  json_encode( $output );

    }

    public function updateData() {
    	$kdatribut = $this->input->post('kdatribut');
    	$faktif = $this->input->post('faktif');
    	$nmatribut = $this->input->post('nmatribut');

    			$q = $this->db->query("UPDATE atribut set nmatribut='".$nmatribut."', faktif='".$faktif."' WHERE kdatribut='".$kdatribut."'");
    		echo $this->db->last_query();

    }

    public function delete(){
    		$kdatribut = $this->input->post('kdatribut');

    		$q = $this->db->query("DELETE FROM atribut WHERE kdatribut='".$kdatribut."'");
    		//echo $this->db->last_query();

    }
}
