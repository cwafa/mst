<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */

?> 
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data');
            echo form_open($submit,$attributes);
        ?>
          <div class="box-header">
            <div class="col-md-6" style="padding: 0;">
              <button type="button" class="form-control btn btn-primary btn-tambah" data-toggle="modal" data-target="#modal_transaksi"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
            </div>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="list-data" class="table table-bordered table-striped dataTable">
                <thead>
                  <tr>
                    <th style="text-align: center;width:20px">No.</th>
                    <th style="text-align: center;">Nama Atribut</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;width:50px">Edit</th>
                    <th style="text-align: center;width:50px">Delete</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>
</div>

<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 style="display:block; text-align:center;">Tambah Data Atribute</h3>

          <form id="form-tambah-atribute">
              <div class="input-group form-group">
                <span class="input-group-addon" id="sizing-addon2">
                  <i class="glyphicon glyphicon-user"></i>
                </span>
                <input type="hidden" class="form-control" name="kdatribut" id="kdatribut" aria-describedby="sizing-addon2" readonly>
                <input type="text" class="form-control" placeholder="Atribute" name="nmatribut" id="nmatribut" aria-describedby="sizing-addon2">
              </div>
              <div class="form-group">
                  <div class="check1">
                      <h5 style="display:block; text-align:left;;">Status Atribut</h5>
                      <input type="checkbox" class="custom-control-input" placeholder="Atribute" name="faktif" id="faktif">
                  </div>
              </div>
            <div class="form-group">
              <div class="col-md-12">
                  <button type="button" class="form-control btn btn-primary btn-add"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
                  <button type="button" class="form-control btn btn-primary btn-update"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
              </div>
            </div>
          </form>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
          $(".btn-add").show();
          $(".btn-update").hide();
          $(".check1").hide();

          $('#faktif').bootstrapToggle({
              on: 'AKTIF',
              off: 'TIDAK AKTIF',
              onstyle: 'success',
              offstyle: 'danger'
          });

          var column = [];

          column.push({
                 "aTargets": [ 1,2,3 ],
                 "searchable": false,
                 "orderable": false,
                 "sClass": "center",
             });

          table = $('.dataTable').DataTable({
             "aoColumnDefs": column,
             "columns": [
                 { "targets": 0 , "data": null,"sortable": false,
                     render: function (data, type, row, meta) {
                         return meta.row + meta.settings._iDisplayStart + 1;
                     }
                 },
                 { "data": "nmatribut" },
                 { "data": "faktifx" },
                 { "data": "edit" },
                 { "data": "delete" }
             ],
             //"lengthMenu": [[ -1], [ "Semua Data"]],
             //"lengthMenu": [[10,25,50, -1], [10,25,50, "Semua Data"]],
             //"bPaginate": true,
             "bProcessing": true,
             "bServerSide": true,
             "bDestroy": true,
             "bAutoWidth": false,
             "fnServerData": function ( sSource, aoData, fnCallback ) {
                 aoData.push(
                          //       { "name": "tahun", "value": $("#tahun").val() }
                             );
                 $.ajax( {
                     "dataType": 'json',
                     "type": "GET",
                     "url": sSource,
                     "data": aoData,
                     "success": fnCallback
                 } );
             },
             'rowCallback': function(row, data, index){
                 //if(data[23]){
                     //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                 //}
             },
             "sAjaxSource": "<?=site_url('atribut/json_dgview');?>",
             "oLanguage": {
                 "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
             },
             //dom: '<"html5buttons"B>lTfgitp',

             buttons: [
                 {extend: 'copy',
                     exportOptions: {orthogonal: 'export'}},
                 {extend: 'csv',
                     exportOptions: {orthogonal: 'export'}},
                 {extend: 'excel',
                     exportOptions: {orthogonal: 'export'}},
                 {extend: 'pdf',
                     orientation: 'landscape',
                     pageSize: 'A0'
                 },
                 {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                 }
             ],

             "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
          });

          $('.dataTable').tooltip({
             selector: "[data-toggle=tooltip]",
             container: "body"
          });

          table.columns().every( function () {
             var that = this;
             $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                 //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                     that
                         .search( this.value )
                         .draw();
                 //}
             } );
          });

           $(".btn-tambah").click(function(){
             $('.btn-add').show();
             $('.btn-update').hide();
             $(".check1").hide();
             clear();
           });

           $(".btn-add").click(function(){
             addData();
           });

           $(".btn-update").click(function(){
             updateData();
           });
    });


    function clear(){
        $("#kdatribut").val('');
        $("#nmatribut").val('');
    }


    function addData(){
          var kdatribut = $("#kdatribut").val();
          var nmatribut = $("#nmatribut").val();
          $.ajax({
              type: "POST",
              url: "<?=site_url("atribut/addData");?>",
              data: {"kdatribut":kdatribut
                      ,"nmatribut":nmatribut},
              success: function(){
                  $("#modal_transaksi").modal('hide');
                          swal({
                              title: "Proses Tambah Atribut Berhasil",
                              text: "Data "+nmatribut+"",
                              type: "success"
                          });
                          table.ajax.reload();
              },
              error:function(event, textStatus, errorThrown) {
                  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  table.ajax.reload();
              }
          });
    }

    function updateData(){
          var kdatribut = $("#kdatribut").val();
          var nmatribut = $("#nmatribut").val();
          if ($('#faktif').prop('checked')){
              var faktif = "true";
          } else {
              var faktif = "false";
          }
          $.ajax({
              type: "POST",
              url: "<?=site_url("atribut/updateData");?>",
              data: {"kdatribut":kdatribut
                      ,"nmatribut":nmatribut
                      ,"faktif":faktif },
              success: function(){
                  $("#modal_transaksi").modal('hide');
                          swal({
                              title: "Proses Update Berhasil",
                              text: "Data "+nmatribut+"",
                              type: "success"
                          });
                          table.ajax.reload();
              },
              error:function(event, textStatus, errorThrown) {
                  swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                  table.ajax.reload();
              }
          });
    }

    function edit(kdatribut,nmatribut,faktifx){

        $(".check1").show();
        $("#kdatribut").val(kdatribut);
        $("#nmatribut").val(nmatribut);
        $("#faktif").val(faktifx);
        if($("#faktif").val()=='AKTIF'){
          $('#faktif').bootstrapToggle('on');
        } else  {
        $('#faktif').bootstrapToggle('off');
        }
        $('#modal_transaksi').modal('toggle');
            $(".btn-add").hide();
            $(".btn-update").show();

        }

    function deleted(kdatribut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url("atribut/delete");?>",
                    data: {"kdatribut":kdatribut },
                    success: function(resp){
                                swal({
                                    title: "Proses Hapus Berhasil",
                                    text: "Data Berhasil Dihapus",
                                    type: "success"
                                });
                        table.ajax.reload();
                    },
                    error:function(event, textStatus, errorThrown) {
                        swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                    }
                });
        });
    }
</script>
