<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Atribut
 *
 * @author adi
 */
class Atribut extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('atribut/submit'),
            'add' => site_url('atribut/add'),
            'edit' => site_url('atribut/edit'),
            'reload' => site_url('atribut'),
        );
        $this->load->model('atribut_qry');
    }

    //redirect if needed, otherwise display the user list
    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }  

  	public function json_dgview() {
  			echo $this->atribut_qry->json_dgview();
  	}

  	public function addData() {
      echo $this->atribut_qry->addData();
  	}

  	public function updateData() {
      echo $this->atribut_qry->updateData();
  	}

  	public function delete() {
      echo $this->atribut_qry->delete();
  	}
}
