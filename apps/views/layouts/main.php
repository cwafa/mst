<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php echo $template['title'];?> </title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url('assets/dist/img/kzu-small.png');?>" type="image/png">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('assets/font-awesome-4.7.0/css/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url('assets/ionicons-2.0.1/css/ionicons.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/flat/blue.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/daterangepicker/daterangepicker.css');?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">

    <!-- jQuery 2.2.3 -->
    <script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?=base_url('assets/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/dataTables.min.css');?>">
    <script src="<?=base_url('assets/plugins/dtables/dataTables.min.js');?>"></script>
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/buttons.dataTables.min.css');?>"></link>
    <script src="<?=base_url('assets/plugins/dtables/dataTables.buttons.min.js');?>"></script>
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/select.dataTables.min.css');?>"></link>
    <script src="<?=base_url('assets/plugins/dtables/dataTables.select.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/fnReloadAjax.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/currency.js');?>"></script>

<!--    <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.buttons.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>-->

    <!-- Sweetalert -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/sweetalert/sweetalert.css');?>">
    <script src="<?=base_url('assets/plugins/sweetalert/sweetalert.min.js');?>"></script>

    <!-- Jansy -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css');?>">
    <script src="<?=base_url('assets/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js');?>"></script>

    <!-- Ajax Form -->
    <script src="<?=base_url('assets/plugins/jquery-form/jquery.form.min.js');?>"></script>

    <!-- AdminLTE App -->
    <script src="<?=base_url('assets/dist/js/app.min.js');?>"></script>

    <!-- Chosen -->
    <link href="<?=base_url('assets/plugins/chosen/bootstrap-chosen.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/chosen/chosen.jquery.js');?>"></script>

    <!-- Select2 -->
    <link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>

    <!-- Numeral JS -->
    <script src="<?=base_url('assets/plugins/numeral/numeral.min.js');?>"></script>

    <!-- Animate -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/animate.css');?>">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">

    <!-- Custom Js CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/my.css');?>">
    <script src="<?=base_url('assets/custom/my.js');?>"></script>

    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/2.0.2/autoNumeric.min.js"></script>-->
    <script src="<?=base_url('assets/dist/js/autoNumeric.min.js');?>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js');?>"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js');?>"></script>
    <![endif]-->
    <style type="text/css">
        .sidebar-menu>li>a {
            border-left: 3px solid transparent;
            font-weight: 300 !important;
        }
        .sidebar-menu>li:hover>a,
        .sidebar-menu>li.active>a {
            background: #f4f4f5;
            font-weight: 600 !important;
        }
        .sidebar-menu>li.active>a {
            color: #fff;
            background: #3c8dbc !important;
            border-left-color: #3c8dbc !important;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=site_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
<span class="logo-mini"><img src="<?=base_url('assets/dist/img/kzu-small.png');?>" style="height: 40px;" /></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
          <?php echo $this->apps->title;?>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a href="javascript:void(0);" class="date-time"></a>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url('assets/dist/img/kzu-small.png');?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('nmuser');?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?=base_url('assets/dist/img/kzu-small.png');?>" class="img-circle" alt="User Image">

                  <p>
                      <?php echo $this->session->userdata('nmuser');?>
                      <small><?php echo $this->apps->logintag;?></small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                        <a href="#">&nbsp;</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
  <!--                  <a href="<?=site_url('profile');?>" class="btn btn-default btn-flat">Profile</a>-->
                  </div>
                  <div class="pull-right">
                      <?php
                          $attributes = array(
                              'id' => 'logout_form'
                              , 'name' => 'logout_form'
                              , 'method' => 'post');
                          echo form_open(site_url('access/logout'),$attributes);

                          $btn_logout = array(
                              'name'          => 'button',
                              'id'            => 'button',
                              'value'         => 'true',
                              'type'          => 'submit',
                              'content'       => '<i class="fa fa-sign-out"></i> Keluar',
                              'class'         => 'btn btn-danger btn-flat'
                          );
                          echo form_button($btn_logout);
                          echo form_close();
                      ?>
                  </div>
                </li>
              </ul>
            </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="padding-bottom: 50px;">
        <div class="pull-left info" style="left: 0px;">
          <p>
              <?php echo $this->session->userdata('nmuser');?>
          </p>
          <a href="#" style="font-size: 10px;"><i class="fa fa-map-marker"></i> <?php echo $this->apps->logintag;?></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php
            $url = array(
                0 => array(
                    'name' => '<fa class="fa fa-dashboard"></fa> <span>Dashboard</span>',
                    'url' => site_url('dashboard'),
                    'class' => 'dashboard',
                ),
                1 => array(
                    'name' => '<fa class="fa fa-file-excel-o"></fa> <span>Atribut</span>',
                    'url' => site_url('atribut'),
                    'class' => 'atribut',
                ),
                2 => array(
                    'name' => '<fa class="fa fa-tags"></fa> <span>Data Pajak</span>',
                    'url' => site_url('mstpajak'),
                    'class' => 'mstpajak',
                ),
                3 => array(
                    'name' => '<fa class="fa fa-tags"></fa> <span>Laporan Pajak</span>',
                    'url' => site_url('rptpajak'),
                    'class' => 'rptpajak',
                )
            );

            $class = $this->uri->segment(1);
            foreach ($url as $value) {
                if($class==$value["class"]){
                    echo '<li class="active"><a href="'.$value["url"].'">'.$value["name"].'</a></li>';
                }else{
                    echo '<li class=""><a href="'.$value["url"].'">'.$value["name"].'</a></li>';
                }
            }
        ?>

        <li>
            <a href="javascript:void(0);" onclick="logout();" title="Keluar dari aplikasi menuju halaman login">
                <i class="fa fa-sign-out"></i>
                <span>Keluar</span>
            </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {msg_main}
        <small>{msg_detail}</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo $template['body'];?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <?php
            echo  (ENVIRONMENT === 'development') ?
                ''
                . 'Memory usage : '
                . $this->benchmark->memory_usage()
                . ' / '
                . $this->benchmark->elapsed_time()
                . ' seconds | '
                . 'CodeIgniter Version '
                . CI_VERSION
                . ' | Engine Ver : ' . phpversion()
                : '' ?>
    </div>
    <div>
        <?php echo $this->apps->copyright;?> &copy; 2017 - <?php echo (date('Y'));?>
    </div>
  </footer>
<script>
    $(document).ready(function() {

        $(".form-control").keyup(function(){
            var val = $(this).val();
            $(this).val(val.toUpperCase());
        });

        setInterval(function(){
            var tanggal = moment().format('DD MMMM YYYY, H:mm:ss');
            $(".date-time").html(tanggal);
        }, 1000);


        $(".chosen-select").chosen({
            no_results_text: "Maaf, data tidak ditemukan!"
        });

        $('.calendar').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('.month').datepicker({
            startView: "year",
            minViewMode: "months",
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "mm-yyyy"
        });
    });

    function logout(){
        swal({
            title: "Konfirmasi Keluar Aplikasi !",
            text: "Pilih Ya, jika sudah selesai menggunakan aplikasi",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Keluar!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            location.replace("<?=site_url('access/logout');?>");
        });
    }
</script>
</div>
<!-- ./wrapper -->
</body>
</html>
