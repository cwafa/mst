﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $template['title'];?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url('assets/dist/img/kzu-small.png');?>" type="image/png">

    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/square/blue.css');?>">
    <!-- Animate -->
    <link rel="stylesheet" href="<?=base_url('assets/custom/animate.css');?>">
    <style type="text/css">
        .img-login{
            background-color: #f1ebeb;
            height: 250px;
            transform: rotate(-10deg);
            border: solid 5px #fff;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.3);
        }

        .login-logo, .register-logo {
            margin-bottom: 0px;
        }

        .login-box, .register-box {
            width: 320px;
            margin: 2% auto;
        }

        .login-page{
            background: #3378a0;
            background-size: 1000% 1000%;
            height: auto;
        }

        .login-box-body, .register-box-body{
            background: none;
        }

        .login-logo a, .register-logo a{
            color: #fff;
        }

        .login-box-msg, .register-box-msg {
            color: #fff;
        }

        @media (max-width: 768px){
            .img-side{
                display: none;
            }
        }
    </style>
</head>

<body class="hold-transition login-page">
    <?php echo $template['body'];?>
    <!-- jQuery 2.2.3 -->
    <script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- iCheck -->
    <script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });

        $(".alert").fadeOut(5000);
      });
    </script>
</body>

</html>
