<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_nmwp'))
{      
    function format_nmwp($nmwp="",$len=28){
        $html = '<table width="100%" border="1" style="border-collapse: collapse;">
        <tr>';
        for($i=0;$i<=$len;$i++){
            if(trim(substr($nmwp,$i,1)) && substr($nmwp,$i,1) !== "."){
                $val = substr($nmwp,$i,1);
            }else if(substr($nmwp,$i,1) == "."){
                $val = substr($nmwp,$i,1);
            }else{
                $val = "&nbsp;";
            }
            
            $html .='<td style="text-align: center;width:22px;">
                '.$val.'
            </td>';
        }
        $html .='</tr>
        </table>';
        return $html;
    }     
}
/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

