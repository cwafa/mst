<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_npwp_21'))
{      
    function format_npwp_21($npwp="000000000000000"){
        $dt = array();
        for($i=0;$i<=strlen($npwp);$i++){
            array_push($dt,substr($npwp,$i,1));
        }
        $html = '<table width="100%"  style="border-collapse: collapse;">
        <tr> <u>
            <td style="text-align: center;width:16px;">
                '.$dt[0].'
            </td>
            <td style="text-align: center;width:16px;">
               '.$dt[1].'
            </td>
            <td style="text-align: center;border: none;width:16px;">
                .
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[2].'
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[3].'
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[4].'
            </td>
            <td style="text-align: center;border: none;width:16px;">
                .
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[5].'
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[6].'
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[7].'
            </td>
            <td style="text-align: center;border: none;width:16px;">
                .
            </td>
            <td style="text-align: center;width:16px;">
                '.$dt[8].'
            </td> </u>
            <td style="text-align: center;border: none;width:16px;">
                &nbsp;
            </td>
            <td style="text-align: center;border: none;width:16px;">
                &nbsp;
            </td>
            <td style="text-align: center;border: none;width:16px;">
                &nbsp;
            </td>
            <td style="text-align: center;border: none;width:16px;">
               - 
            </td> 
         ';
        for($i=1;$i<=9;$i++){
            $html .='<td style="text-align: center;border: none;width:16px;">
                        &nbsp;
                    </td>';
        }
        $html .='</tr>
        </table>';
        return $html;                
    }        
}


