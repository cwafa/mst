<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_npwp_2'))
{      
    function format_npwp_2($npwp="000000000000000"){
        $dt = array();
        for($i=0;$i<=strlen($npwp);$i++){
            array_push($dt,substr($npwp,$i,1));
        }
        $html = '<table width="100%" border="1" style="border-collapse: collapse;">
        <tr>
            <td style="text-align: center;width:22px;">
                '.$dt[0].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[1].'
            </td>
            <td style="text-align: center;border: none;width:22px;">
                -
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[2].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[3].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[4].'
            </td>
            <td style="text-align: center;border: none;width:22px;">
                -
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[5].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[6].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[7].'
            </td>
            <td style="text-align: center;border: none;width:22px;">
                -
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[8].'
            </td>
            <td style="text-align: center;border: none;width:22px;">
                -
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[9].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[10].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[11].'
            </td>
            <td style="text-align: center;border: none;width:22px;">
                -
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[12].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[13].'
            </td>
            <td style="text-align: center;width:22px;">
                '.$dt[14].'
            </td>';
        for($i=1;$i<=9;$i++){
            $html .='<td style="text-align: center;border: none;width:22px;">
                        &nbsp;
                    </td>';
        }
        $html .='</tr>
        </table>';
        return $html;                
    }        
}
/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

