-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2020 at 06:43 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cendana`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`, `foto`) VALUES
(1, 'auwfar', 'f0a047143d1da15b630c73f0256d5db0', 'Achmad Chadil Auwfar', 'Koala.jpg'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'boy-512.png');

-- --------------------------------------------------------

--
-- Table structure for table `atribut`
--

CREATE TABLE `atribut` (
  `kdatribut` int(11) NOT NULL,
  `nmatribut` varchar(50) DEFAULT NULL,
  `faktif` enum('true','false') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atribut`
--

INSERT INTO `atribut` (`kdatribut`, `nmatribut`, `faktif`) VALUES
(1, 'Hotel', 'true'),
(2, 'Restoran', 'true'),
(3, 'Hiburan', 'true'),
(4, 'Reklame', 'true'),
(5, 'Penerangan', 'true'),
(6, 'Mineral', 'true'),
(7, 'Parkir', 'true'),
(8, 'Air Tanah', 'true'),
(9, 'Walet', 'true'),
(10, 'Pbhtb', 'true'),
(11, 'Pbb', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `d2_attr`
--

CREATE TABLE `d2_attr` (
  `kddata` int(11) NOT NULL,
  `kdatribut` int(10) NOT NULL,
  `tg_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `rl_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `ket` varchar(25) NOT NULL,
  `periode` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d2_attr`
--

INSERT INTO `d2_attr` (`kddata`, `kdatribut`, `tg_atribut`, `rl_atribut`, `ket`, `periode`) VALUES
(1, 1, '56356000000.00', '66809551699.00', 'Tercapai', '2016-07-06'),
(2, 2, '87500000000.00', '89400153784.00', 'Tercapai', '2016-07-06'),
(3, 3, '17000000000.00', '17569777486.00', 'Tercapai', '2016-07-06'),
(4, 4, '28135001000.00', '29156573475.00', 'Tercapai', '2016-07-06'),
(5, 5, '186000000000.00', '189895422761.00', 'Tercapai', '2016-07-06'),
(6, 6, '200000000.00', '105999850.00', 'Tidak Tercapai', '2016-07-06'),
(7, 7, '10000000000.00', '11390247548.00', 'Tercapai', '2016-07-06'),
(8, 8, '5343750000.00', '6408836540.00', 'Tercapai', '2016-07-06'),
(9, 9, '50000000.00', '750000.00', 'Tidak Tercapai', '2016-07-06'),
(10, 10, '254505000000.00', '318876347267.00', 'Tercapai', '2016-07-06'),
(11, 11, '241875000000.00', '259330711649.00', 'Tercapai', '2016-07-06'),
(12, 1, '69500000000.00', '72041787841.00', 'Tercapai', '2017-07-06'),
(13, 2, '107500000000.00', '111617284979.00', 'Tercapai', '2017-07-06'),
(14, 3, '20500000000.00', '22156079295.00', 'Tercapai', '2017-07-06'),
(15, 4, '31000000000.00', '28899109922.00', 'Tidak Tercapai', '2017-07-06'),
(16, 5, '205000000000.00', '208428629152.00', 'Tercapai', '2017-07-06'),
(17, 6, '200000000.00', '330660125.00', 'Tercapai', '2017-07-06'),
(18, 7, '15000000000.00', '15176480717.00', 'Tercapai', '2017-07-06'),
(19, 8, '8000000000.00', '8144245075.00', 'Tercapai', '2017-07-06'),
(20, 9, '50000000.00', '1020000.00', 'Tidak Tercapai', '2017-07-06'),
(21, 10, '320000000000.00', '416395327140.00', 'Tercapai', '2017-07-06'),
(22, 11, '335000000000.00', '348354499317.00', 'Tercapai', '2017-07-06'),
(23, 1, '1.00', '2.00', 'Tercapai', '2020-07-08'),
(24, 2, '122.00', '33.00', 'Tidak Tercapai', '2020-07-08'),
(25, 3, '18999.00', '102002.00', 'Tercapai', '2020-07-13'),
(26, 5, '23333.00', '34322.00', 'Tercapai', '2020-07-13');

-- --------------------------------------------------------

--
-- Table structure for table `d3_attr`
--

CREATE TABLE `d3_attr` (
  `kddata` int(11) NOT NULL,
  `kdatribut` int(10) NOT NULL,
  `tg_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `rl_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `ket` varchar(25) NOT NULL,
  `periode` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d3_attr`
--

INSERT INTO `d3_attr` (`kddata`, `kdatribut`, `tg_atribut`, `rl_atribut`, `ket`, `periode`) VALUES
(1, 1, '56356000000.00', '66809551699.00', 'Tercapai', '2016-07-06'),
(2, 2, '87500000000.00', '89400153784.00', 'Tercapai', '2016-07-06'),
(3, 3, '17000000000.00', '17569777486.00', 'Tercapai', '2016-07-06'),
(4, 4, '28135001000.00', '29156573475.00', 'Tercapai', '2016-07-06'),
(5, 5, '186000000000.00', '189895422761.00', 'Tercapai', '2016-07-06'),
(6, 6, '200000000.00', '105999850.00', 'Tidak Tercapai', '2016-07-06'),
(7, 7, '10000000000.00', '11390247548.00', 'Tercapai', '2016-07-06'),
(8, 8, '5343750000.00', '6408836540.00', 'Tercapai', '2016-07-06'),
(9, 9, '50000000.00', '750000.00', 'Tidak Tercapai', '2016-07-06'),
(10, 10, '254505000000.00', '318876347267.00', 'Tercapai', '2016-07-06'),
(11, 11, '241875000000.00', '259330711649.00', 'Tercapai', '2016-07-06'),
(12, 1, '69500000000.00', '72041787841.00', 'Tercapai', '2017-07-06'),
(13, 2, '107500000000.00', '111617284979.00', 'Tercapai', '2017-07-06'),
(14, 3, '20500000000.00', '22156079295.00', 'Tercapai', '2017-07-06'),
(15, 4, '31000000000.00', '28899109922.00', 'Tidak Tercapai', '2017-07-06'),
(16, 5, '205000000000.00', '208428629152.00', 'Tercapai', '2017-07-06'),
(17, 6, '200000000.00', '330660125.00', 'Tercapai', '2017-07-06'),
(18, 7, '15000000000.00', '15176480717.00', 'Tercapai', '2017-07-06'),
(19, 8, '8000000000.00', '8144245075.00', 'Tercapai', '2017-07-06'),
(20, 9, '50000000.00', '1020000.00', 'Tidak Tercapai', '2017-07-06'),
(21, 10, '320000000000.00', '416395327140.00', 'Tercapai', '2017-07-06'),
(22, 11, '335000000000.00', '348354499317.00', 'Tercapai', '2017-07-06'),
(23, 1, '1.00', '2.00', 'Tercapai', '2020-07-08'),
(24, 2, '122.00', '33.00', 'Tidak Tercapai', '2020-07-08'),
(25, 3, '18999.00', '102002.00', 'Tercapai', '2020-07-13'),
(26, 5, '23333.00', '34322.00', 'Tercapai', '2020-07-13');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `kdatribut` varchar(15) NOT NULL DEFAULT '',
  `nmatribut` varchar(20) NOT NULL,
  `tg_atribut` decimal(15,2) NOT NULL DEFAULT '0.00',
  `rl_atribut` decimal(15,2) NOT NULL DEFAULT '0.00',
  `ket` varchar(50) NOT NULL,
  `tgl_masuk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`kdatribut`, `nmatribut`, `tg_atribut`, `rl_atribut`, `ket`, `tgl_masuk`) VALUES
('1', 'Hutan', '12345567.00', '99999999.99', 'Tercapai', '2020-07-06'),
('10', '9', '50000000.00', '750000.00', 'Tidak Tercapai', '2016-07-06'),
('11', '10', '254505000000.00', '318876347267.00', 'Tercapai', '2016-07-06'),
('12', '11', '241875000000.00', '259330711649.00', 'Tercapai', '2016-07-06'),
('13', '1', '69500000000.00', '72041787841.00', 'Tercapai', '2017-07-06'),
('14', '2', '107500000000.00', '111617284979.00', 'Tercapai', '2017-07-06'),
('15', '3', '20500000000.00', '22156079295.00', 'Tercapai', '2017-07-06'),
('16', '4', '31000000000.00', '28899109922.00', 'Tidak Tercapai', '2017-07-06'),
('17', '5', '205000000000.00', '208428629152.00', 'Tercapai', '2017-07-06'),
('18', '6', '200000000.00', '330660125.00', 'Tercapai', '2017-07-06'),
('19', '7', '15000000000.00', '15176480717.00', 'Tercapai', '2017-07-06'),
('2', '1', '56356000000.00', '66809551699.00', 'Tercapai', '2016-07-06'),
('20', '8', '8000000000.00', '8144245075.00', 'Tercapai', '2017-07-06'),
('21', '9', '50000000.00', '1020000.00', 'Tidak Tercapai', '2017-07-06'),
('22', '10', '320000000000.00', '416395327140.00', 'Tercapai', '2017-07-06'),
('23', '11', '335000000000.00', '348354499317.00', 'Tercapai', '2017-07-06'),
('3', '2', '87500000000.00', '89400153784.00', 'Tercapai', '2016-07-06'),
('4', '3', '17000000000.00', '17569777486.00', 'Tercapai', '2016-07-06'),
('5', '4', '28135001000.00', '29156573475.00', 'Tercapai', '2016-07-06'),
('6', '5', '186000000000.00', '189895422761.00', 'Tercapai', '2016-07-06'),
('7', '6', '200000000.00', '105999850.00', 'Tidak Tercapai', '2016-07-06'),
('8', '7', '10000000000.00', '11390247548.00', 'Tercapai', '2016-07-06'),
('9', '8', '5343750000.00', '6408836540.00', 'Tercapai', '2016-07-06');

-- --------------------------------------------------------

--
-- Table structure for table `d_attr`
--

CREATE TABLE `d_attr` (
  `kddata` int(11) NOT NULL,
  `kdatribut` int(10) NOT NULL,
  `tg_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `rl_atribut` decimal(20,2) NOT NULL DEFAULT '0.00',
  `ket` varchar(25) NOT NULL,
  `periode` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_attr`
--

INSERT INTO `d_attr` (`kddata`, `kdatribut`, `tg_atribut`, `rl_atribut`, `ket`, `periode`) VALUES
(1, 1, '56356000000.00', '66809551699.00', 'Tercapai', '2016-07-06'),
(2, 2, '87500000000.00', '89400153784.00', 'Tercapai', '2016-07-06'),
(3, 3, '17000000000.00', '17569777486.00', 'Tercapai', '2016-07-06'),
(4, 4, '28135001000.00', '29156573475.00', 'Tercapai', '2016-07-06'),
(5, 5, '186000000000.00', '189895422761.00', 'Tercapai', '2016-07-06'),
(6, 6, '200000000.00', '105999850.00', 'Tidak Tercapai', '2016-07-06'),
(7, 7, '10000000000.00', '11390247548.00', 'Tercapai', '2016-07-06'),
(8, 8, '5343750000.00', '6408836540.00', 'Tercapai', '2016-07-06'),
(9, 9, '50000000.00', '750000.00', 'Tidak Tercapai', '2016-07-06'),
(10, 10, '254505000000.00', '318876347267.00', 'Tercapai', '2016-07-06'),
(11, 11, '241875000000.00', '259330711649.00', 'Tercapai', '2016-07-06'),
(12, 1, '69500000000.00', '72041787841.00', 'Tercapai', '2017-07-06'),
(13, 2, '107500000000.00', '111617284979.00', 'Tercapai', '2017-07-06'),
(14, 3, '20500000000.00', '22156079295.00', 'Tercapai', '2017-07-06'),
(15, 4, '31000000000.00', '28899109922.00', 'Tidak Tercapai', '2017-07-06'),
(16, 5, '205000000000.00', '208428629152.00', 'Tercapai', '2017-07-06'),
(17, 6, '200000000.00', '330660125.00', 'Tercapai', '2017-07-06'),
(18, 7, '15000000000.00', '15176480717.00', 'Tercapai', '2017-07-06'),
(19, 8, '8000000000.00', '8144245075.00', 'Tercapai', '2017-07-06'),
(20, 9, '50000000.00', '1020000.00', 'Tidak Tercapai', '2017-07-06'),
(21, 10, '320000000000.00', '416395327140.00', 'Tercapai', '2017-07-06'),
(22, 11, '335000000000.00', '348354499317.00', 'Tercapai', '2017-07-06'),
(23, 1, '1.00', '2.00', 'Tercapai', '2020-07-08'),
(24, 2, '122.00', '33.00', 'Tidak Tercapai', '2020-07-08'),
(25, 3, '18999.00', '102002.00', 'Tercapai', '2020-07-13'),
(26, 5, '23333.00', '34322.00', 'Tercapai', '2020-07-13');

-- --------------------------------------------------------

--
-- Table structure for table `m_attr`
--

CREATE TABLE `m_attr` (
  `kdatribut` int(10) NOT NULL,
  `nmatribut` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `notelp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_attr`
--

INSERT INTO `m_attr` (`kdatribut`, `nmatribut`, `alamat`, `notelp`) VALUES
(1, 'hotel', '-', '-'),
(2, 'pabrik', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `total`
--

CREATE TABLE `total` (
  `id` int(11) NOT NULL,
  `kd` varchar(255) DEFAULT NULL,
  `nm` varchar(255) DEFAULT NULL,
  `t1` varchar(255) DEFAULT NULL,
  `r1` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `t2` varchar(255) DEFAULT NULL,
  `r2` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total`
--

INSERT INTO `total` (`id`, `kd`, `nm`, `t1`, `r1`, `k1`, `t2`, `r2`, `k2`, `ket`) VALUES
(1250, '1', 'Hotel', '69500000000.00', '72041787841.00', 'Tercapai', '56356000000.00', '66809551699.00', 'Tercapai', 'tc'),
(1251, '2', 'Restoran', '107500000000.00', '111617284979.00', 'Tercapai', '87500000000.00', '89400153784.00', 'Tercapai', 'tc'),
(1252, '3', 'Hiburan', '20500000000.00', '22156079295.00', 'Tercapai', '17000000000.00', '17569777486.00', 'Tercapai', 'tc'),
(1253, '4', 'Reklame', '31000000000.00', '28899109922.00', 'Tidak Tercapai', '28135001000.00', '29156573475.00', 'Tercapai', 'tt'),
(1254, '5', 'Penerangan', '205000000000.00', '208428629152.00', 'Tercapai', '186000000000.00', '189895422761.00', 'Tercapai', 'tc'),
(1255, '6', 'Mineral', '200000000.00', '330660125.00', 'Tercapai', '200000000.00', '105999850.00', 'Tidak Tercapai', 'tc'),
(1256, '7', 'Parkir', '15000000000.00', '15176480717.00', 'Tercapai', '10000000000.00', '11390247548.00', 'Tercapai', 'tc'),
(1257, '8', 'Air Tanah', '8000000000.00', '8144245075.00', 'Tercapai', '5343750000.00', '6408836540.00', 'Tercapai', 'tc'),
(1258, '9', 'Walet', '50000000.00', '1020000.00', 'Tidak Tercapai', '50000000.00', '750000.00', 'Tidak Tercapai', 'tt'),
(1259, '10', 'Pbhtb', '320000000000.00', '416395327140.00', 'Tercapai', '254505000000.00', '318876347267.00', 'Tercapai', 'tc'),
(1260, '11', 'Pbb', '335000000000.00', '348354499317.00', 'Tercapai', '241875000000.00', '259330711649.00', 'Tercapai', 'tc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atribut`
--
ALTER TABLE `atribut`
  ADD PRIMARY KEY (`kdatribut`);

--
-- Indexes for table `d2_attr`
--
ALTER TABLE `d2_attr`
  ADD PRIMARY KEY (`kddata`);

--
-- Indexes for table `d3_attr`
--
ALTER TABLE `d3_attr`
  ADD PRIMARY KEY (`kddata`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`kdatribut`);

--
-- Indexes for table `d_attr`
--
ALTER TABLE `d_attr`
  ADD PRIMARY KEY (`kddata`);

--
-- Indexes for table `m_attr`
--
ALTER TABLE `m_attr`
  ADD PRIMARY KEY (`kdatribut`);

--
-- Indexes for table `total`
--
ALTER TABLE `total`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `atribut`
--
ALTER TABLE `atribut`
  MODIFY `kdatribut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `d2_attr`
--
ALTER TABLE `d2_attr`
  MODIFY `kddata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `d3_attr`
--
ALTER TABLE `d3_attr`
  MODIFY `kddata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `d_attr`
--
ALTER TABLE `d_attr`
  MODIFY `kddata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `m_attr`
--
ALTER TABLE `m_attr`
  MODIFY `kdatribut` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `total`
--
ALTER TABLE `total`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1265;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
